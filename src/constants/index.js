export default {
  // Vuex object is saved into the localstorage with this name
  KEY_VUEX_LOCALSTORAGE: 'aquasig',
  // Path where is saved the token into the localstorage
  PATH_TOKEN_LOCALSTORAGE: 'token.value',

  // Vuex modules
  VUEX_MODULES: {
    alert: 'alert',
    loadingLayer: 'loadingLayer',
    modal: 'modal',
    user: 'user',
    scope: 'scope',
    issue: 'issue'
  },

  // Flow app
  INIT_APP: 'issues',
  LOGIN_PATH: 'login',

  // DOM elements
  APP_ID: 'app',
  APP_MAIN_ALERT_ID: 'app-main-alert',
  APP_MAIN_MODAL_ID: 'app-main-modal',
  APP_MAIN_LOADING_ID: 'app-main-loading-layer',

  // Language
  DEFAULT_LANGUAGE: 'es',
  AVAILABLE_LANGUAGES: ['en', 'es']
}
