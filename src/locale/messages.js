export default {
  es: {
    // Acciones varias
    action: {
      error: {
        default: 'Algo fallo!!!',
        loadImage: 'No se pudo cargar la imagen',
        position: 'No se pudo conseguir la posición del usuario',
        server: 'Hubo un error en la respuesta del servidor'
      },
      user: {
        closeSession: 'Cerrar sesión',
        sessionClosed: 'Sesión cerrada'
      }
    },
    // Textos varios de la aplicación
    app: {
      all: 'Todos',
      downloadApk: 'La APK de Android se descargá en breve...',
      info: {
        title: 'Información'
      },
      label: {
        brand: 'Mantenimiento'
      },
      map: {
        infoView: {
          area: 'Area',
          capacity: 'Capacidad',
          connection: 'Acometida',
          consumption: 'Consumo',
          consumptionTotal: 'Consumo total',
          consumptionDaily: 'Consumo diario',
          floors: 'Plantas',
          forecast: 'previsión',
          hydrant: 'Hidrante',
          location: 'Localización',
          name: 'Nombre',
          dissolvedOxygen: 'Oxígeno disuelto',
          electricConductivity: 'Conductividad eléctrica',
          ph: 'PH',
          plot: 'Parcela',
          reference: 'Referencia',
          registryRef: 'Identificador catastral',
          sector: 'Sector',
          sensor: 'Sensor',
          status: 'Estado',
          tank: 'Depósito',
          temperature: 'Temperatura',
          valve: 'Válvula',
          well: 'Pozo'
        },
        seeAround: 'Ver alrededor'
      },
      myself: 'Yo mismo',
      nav: {
        explore: 'Explorar',
        issues: 'Órdenes',
        new: 'Nueva'
      },
      time: {
        days: 'días',
        hours: 'horas',
        minutes: 'minutos',
        months: 'meses',
        weeks: 'semanas',
        human: {
          days: 'Hace {time} días',
          hours: 'Hace {time} horas',
          minutes: 'Hace {time} minutos',
          months: 'Hace {time} meses',
          weeks: 'Hace {time} semanas',
          zero: 'Menos de un minuto'
        }
      }
    },
    // Formularios
    form: {
      action: {
        login: 'Acceso',
        save: 'Guardar',
        search: 'Buscar',
        cancel: 'Cancelar',
        errorUploadSize: 'La imagen (o el total de imágenes) excede el tamaño máximo permitido, no se incluirá'
      },
      fields: {
        address: 'Dirección',
        assignedTo: 'Asignadas a',
        budget: 'Presupuesto',
        dateFinish: 'Hasta',
        dateStart: 'Desde',
        description: 'Descripción del trabajo',
        estimatedTime: 'Tiempo aproximado',
        email: 'Email',
        files: 'Ficheros',
        filesToUpload: 'Ficheros que se van a subir',
        idEntity: 'Nº orden',
        password: 'Contraseña',
        statusLabel: 'Estado',
        type: 'Tipo',
        uploadFiles: 'Incluye un fichero o toma una foto',
        assignedUser: 'Técnico asignado'
      },
      validation: {
        decimal: 'Debe indicar un número con decimales',
        files: 'El tamaño del fichero no puede exceder de 200Kb y los tipos soportados son JPG, GIF, PNG y PDF',
        format: 'El formato es incorrecto',
        notEmpty: 'No puede quedar vacío',
        numeric: 'Debe indicar un número entero',
        selectOne: 'Debe seleccionar una opcíon'
      },
      info: {
        budget: 'El precio es en euros (€)',
        estimatedTime: 'El tiempo se estimará en días'
      }
    },
    // Vistas
    view: {
      user: {
        login: {
          title: 'AquaSig',
          errorForm: 'El email o contraseña no son correctos'
        }
      },
      scope: {
        form: {
          currentScope: 'Ámbito actual',
          selectScope: '-- Seleccione un ámbito --',
          titleModal: 'Seleccione su ámbito'
        }
      },
      issue: {
        addFiles: 'Añadir ficheros',
        addStatus: 'Añadir estado',
        address: 'Dirección',
        assignedUser: 'Técnico asignado',
        budget: 'Presupuesto',
        characteristics: 'Característica',
        created: 'Creada',
        currentStatus: 'Estado actual',
        date: 'Fecha',
        description: 'Descripción',
        estimatedTime: 'Tiempo estimado',
        documentsAttached: 'Documentos asociados',
        imagesAttached: 'Imágenes asociadas',
        form: {
          edit: {
            errorForm: 'Ocurrio un error al intertar guardar la tarea',
            invalidForm: 'Complete todos campos obligatorios',
            new: 'Nueva orden',
            selectStatus: '-- Seleccione un estado --',
            successForm: 'La tarea se ha salvado correctamente',
            selectType: '-- Seleccione un tipo --',
            selectUser: '-- Seleccione un técnico --'
          },
          files: {
            successForm: 'Los ficheros fueron incluidos correctamente',
            errorForm: 'No se pudieron incluir los ficheros'
          },
          status: {
            successForm: 'El estado se ha actualizado correctamente',
            errorForm: 'No se pudo actualizar el estado'
          }
        },
        name: 'Órdenes',
        noResult: 'No se han encontrado trabajos que realizar',
        number: 'Nº orden',
        scope: {
          notSelected: 'Debe seleccionar un ambito para mostrar las ordenes asociadas'
        },
        numRegisters: '{number} registros encontrados',
        removeQuestion: '¿Estás seguro que deseas eliminar la tarea?',
        searcher: 'Buscador',
        seeInMap: 'Ver en mapa',
        status: {
          closed: 'Cerrada',
          in_progress: 'En progreso',
          incident: 'Incidente',
          leak: 'Fuga',
          registered: 'Registrado'
        },
        statusLabel: 'Estado',
        table: {
          addButton: 'Añadir estado',
          title: 'Estados de la orden'
        },
        type: 'Tipo',
        types: {
          fault: 'Avería',
          infrastructure_update: 'Actualización de infraestructura',
          leak: 'Fuga',
          maintenance: 'Mantenimiento'
        }
      },
      issueAdd: {
        cancel: 'Cancelar',
        seeAround: 'Explorar',
        selectLocation: 'Seleccionar ubicación',
        setLocation: 'Seleccionar'
      }
    }
  }
}
