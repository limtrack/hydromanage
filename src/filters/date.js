import Vue from 'vue'
// Utils
import utils from '@/lib/utils'

const { dateFormatToLocale } = utils
/**
 * Parse a date to format locale
 * @param {String} date
 * @return {String} - parsed date
 */
Vue.filter('dateFormatToLocale', (date = '') => {
  return dateFormatToLocale(date)
})
