import Vue from 'vue'

// Utils
import utils from '@/lib/utils'

const { nl2br } = utils

/**
 * Truncate string
 * Options availables
 *  - length -> size string to return, default 30
 *  - clamp -> put at the end string, default '...'
 *  - removeHtml -> remove HTML code, default false
 *
 * @param {String} text - strint to transform
 * @param {Object} options - differents options
 * @return {String} - parsed string
 */
Vue.filter('truncate', (text = '', options) => {
  const {
    length,
    clamp,
    removeHtml
  } = Object.assign({ length: 30, clamp: '...', removeHtml: false }, options)

  // Empty string
  if (text === null || typeof text === 'undefined') {
    return ''
  }

  let currentText = typeof text === 'number'
    ? text.toString(10)
    : text

  currentText = removeHtml
    ? currentText.replace(/<[^>]*>/g, '')
    : currentText

  // The string is smaller that maximum length
  if (currentText.length <= length) {
    return currentText
  }

  let tcText = currentText.slice(0, length - clamp.length)
  let last = tcText.length - 1

  while (last > 0 && tcText[last] !== ' ' && tcText[last] !== clamp[0]) {
    last -= 1
  }

  // Fix for case when text dont have any `space`
  last = last || length - clamp.length

  tcText = tcText.slice(0, last)

  return `${tcText}${clamp}`
})

/**
 * Modify the text to put <br> where is new line
 *
 * @param {String} text - string to transform
 * @return {String} - parsed string
 */
Vue.filter('nl2br', (text = '') => {
  return nl2br(text)
})
