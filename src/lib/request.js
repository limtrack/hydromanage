// Library to do request
import axios from 'axios'
import constants from '@/constants'
// THIS IS MAGICAL :D
const get = require('lodash.get')

const { PATH_TOKEN_LOCALSTORAGE, KEY_VUEX_LOCALSTORAGE, LOGIN_PATH } = constants
// API
const API_ENDPOINT = process.env.API_ENDPOINT

/**
 * Do a request to API server
 *
 * @param {Object} options - different options to do a request
 * @param {Booelan} withToken - use token in request?
 * @return {Object} - parameters request header
 */
const setHeaders = (options = {}, withToken = true) => {
  const storeLocalStore = window.localStorage[KEY_VUEX_LOCALSTORAGE]
    ? JSON.parse(window.localStorage[KEY_VUEX_LOCALSTORAGE])
    : {}

  const headers = Object.assign({
    'Content-Type': 'application/json'
  }, options)

  if (withToken !== false) {
    headers['X-Access-Token'] = get(storeLocalStore, PATH_TOKEN_LOCALSTORAGE, '')
  }
  return headers
}

/**
 * Do a request to API server
 *
 * @param {Object} request - params to do request
 * @return {Promise}
 */
const doRequest = (request, withToken = true) => {
  const promiseRequest = new Promise((resolve, reject) => {
    if (typeof request.url === 'undefined' || Boolean(request.url) === false) {
      return reject(new Error('You must indicate an URL'))
    }

    const defaultRequest = {
      method: 'GET',
      timeout: 5000
    }

    const currentRequest = request
    currentRequest.url = currentRequest.url.indexOf(API_ENDPOINT) < 0
      ? `${API_ENDPOINT}${currentRequest.url}`
      : currentRequest.url

    // Add headers
    currentRequest.headers = setHeaders(request.headers || {}, withToken)

    return axios(Object.assign({}, defaultRequest, currentRequest))
      .catch((res) => {
        // Expire token - redirect to login
        if (res.response && res.response.status === 403) {
          window.location.replace(`${window.location.origin}/#/${LOGIN_PATH}`)
        } else {
          reject(new Error('Request failed'))
        }
      })
      .then(resolve)
  })

  return promiseRequest
}

export default doRequest
