import { helpers } from 'vuelidate/lib/validators'

/**
 * Check the size file
 *
 * @param {Object} file - file to check
 * @return {Boolean}
 */
const sizeFile = (file, maxSize = 204800) => {
  // default 200Kb
  return helpers.req(file) && file.size <= maxSize
}

/**
 * Check the type file
 *
 * @param {Object} file - file to check
 * @return {Boolean}
 */
const typeFile = (file, types = ['image/jpeg', 'image/gif', 'image/png', 'application/pdf']) => {
  return helpers.req(file) && types.includes(file.type)
}

export default {
  sizeFile: sizeFile,
  typeFile: typeFile
}
