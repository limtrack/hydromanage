import constants from '@/constants'
import moment from 'moment'

const {
  DEFAULT_LANGUAGE
} = constants

/**
 * Parse a date to UTC
 * @param {String} date
 * @return {String} - parsed date
 */
const dateToUTC = (date = '') => {
  if (!date) {
    return date
  }
  const parseDate = moment.utc(date).toDate()
  moment.locale(DEFAULT_LANGUAGE)
  return moment(parseDate).local().format('LLL')
}

/**
 * Parse a date to format locale
 * @param {String} date
 * @return {String} - parsed date
 */
const dateFormatToLocale = (date = '') => {
  if (!date) {
    return date
  }
  moment.locale(DEFAULT_LANGUAGE)
  return moment(date).format('LLL')
}

/**
 * Open the Google street view App
 * @param {Number} lat - latitude
 * @param {Number} lng - longitude
 */
const goToStreetView = (lat, lng) => {
  window.open(`http://maps.google.com/maps?q=&layer=c&cbll=${lat},${lng}`)
}

/**
 * Transform a base64 string to size file
 * @param {Object} file - file object
 * @return {Number} - size file
 */
const base64ToSizeFile = (base64) => {
  const splitFile = base64.split(',')
  const contentWithoutMime = Array.isArray(splitFile)
    ? splitFile[1]
    : base64

  return window.atob(contentWithoutMime).length
}

/**
 * Transform a file into base64
 * @param {Object} file - file object
 * @return {String} - base64
 */
const fileToBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()

    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = (error) => reject(error)
  })
}

/**
 * Modify the text to put <br> where is new line
 * @param {String} text - text to change
 * @return {String}
 */
const nl2br = (text) => {
  return typeof text === 'undefined' || text === null
    ? ''
    : (text).replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br />' + '$2')
}

/**
 * Using file name, it checks if the file is a image
 * @param {String} name - file name
 * @return {Boolean} ¿is file an image?
 */
const isFileAnImage = (name) => {
  const ext = name.split('.').pop()

  return ['jpg', 'jpeg', 'gif', 'png', 'bmp', 'tiff']
    .indexOf(ext) > -1
}

export default {
  base64ToSizeFile: base64ToSizeFile,
  dateToUTC: dateToUTC,
  dateFormatToLocale: dateFormatToLocale,
  fileToBase64: fileToBase64,
  goToStreetView: goToStreetView,
  isFileAnImage: isFileAnImage,
  nl2br: nl2br
}
