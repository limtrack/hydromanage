// Constants
import constants from '@/constants'
// Lib
import doRequest from '@/lib/request'
// Vuex
import { mapGetters } from 'vuex'

const { VUEX_MODULES } = constants

export default {
  data () {
    return {
      errorPosition: this.$t('action.error.position')
    }
  },
  computed: {
    ...mapGetters(VUEX_MODULES.scope, ['getSelectedScope'])
  },

  methods: {
    /**
     * Get data about "water consumption" status types
     *
     * Possibles values for "entity" are:
     * 'aq_cata.sensor', 'aq_cons.sector', 'aq_cata.tank', 'aq_cata.connections_point',
     * 'aq_cata.supply_line', 'aq_cata.hydrant_point', 'aq_cata.hydrant_line',
     * 'aq_cata.valve_point', 'aq_cata.valve_line', 'aq_cata.well_point',
     * 'aq_cata.well_line', 'aq_cons.plot'
     *
     * Possibles values for "type" are:
     * 'historic', 'now'
     *
     * @param {options} - options to the request URL
     * @param {data} - differents filters and options to the request
     * @return {Promise}
     */
    requestWaterConsumptionApi (options = {}, data = {}) {
      return doRequest({
        url: `/${this.getSelectedScope}/maps/${options.entity}/${options.type}`,
        method: 'POST',
        timeout: 15000,
        data
      }, true)
    },

    /**
     * Get User' geolocation
     *
     * @return {Promise} - with coordinates location user
     */
    getUserLocation () {
      const _this = this
      return new Promise((resolve, reject) => {
        if (navigator.geolocation) {
          navigator.geolocation
            .getCurrentPosition(
              (position) => {
                resolve({
                  longitude: position.coords.longitude,
                  latitude: position.coords.latitude
                })
              },
              () => {
                reject(_this.errorPosition)
              }
            )
        } else {
          reject(_this.errorPosition)
        }
      })
    },
    /**
     * Add array images to map
     *
     * @param {Array} images - Array differents images
     * @param {Object} map - map currently used
     * @return {Promise}
     */
    addImagesMap (images, map) {
      const imagesPromises = []

      images.forEach((image) => {
        imagesPromises.push(this.loadImageInMap(image, map))
      })

      return Promise.all(imagesPromises)
    },
    /**
     * Load the image into the map
     *
     * @param {Object} image - image data
     * @param {Object} map - map currently used
     * @returns {Promise}
     */
    loadImageInMap (image, map) {
      const _this = this

      return new Promise((resolve, reject) => {
        map.loadImage(image.url, (error, loadedImage) => {
          if (error) {
            reject(_this.errorLoadImage)
          }
          if (!map.hasImage(image.id)) {
            map.addImage(image.id, loadedImage)
          }
          resolve()
        })
      })
    }
  }
}
