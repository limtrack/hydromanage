// Constants
import constants from '@/constants'
// Lib
import doRequest from '@/lib/request'
// Mixins
import interfaceMixin from '@/mixins/interfaceMixin'
// Vuex
import issueActionsName from '@/store/modules/issue/actionsName'
import userActionsName from '@/store/modules/user/actionsName'
import { mapGetters } from 'vuex'

const {
  UPDATE_ISSUE_ITEMS,
  UPDATE_ISSUE,
  UPDATE_ISSUE_DELETE,
  UPDATE_ISSUE_FILE,
  UPDATE_ISSUE_STATUS,
  UPDATE_ALL_ISSUE_STATUS,
  UPDATE_ALL_ISSUE_TYPES
} = issueActionsName
const {
  UPDATE_USERS
} = userActionsName
const { VUEX_MODULES } = constants
const get = require('lodash.get')

export default {
  mixins: [ interfaceMixin ],
  data () {
    return {
      errorRequest: this.$t('action.error.server')
    }
  },
  computed: {
    ...mapGetters(VUEX_MODULES.scope, ['getSelectedScope'])
  },
  methods: {
    /**
     * Get users from server
     * @param {Object} params - option for request
     * @return {Promise}
     */
    getUsersApi () {
      return doRequest({
        url: `/users`,
        method: 'GET'
      }, true)
    },

    /**
     * Get Issues from server
     * @param {String} idScope - id scope
     * @param {Object} params - option for request
     * @return {Promise}
     */
    getIssuesApi (idScope, params = {}) {
      return doRequest({
        url: `/${idScope}/maintenance/issues`,
        method: 'GET',
        params
      }, true)
    },

    /**
     * Save Issue to server
     * @param {Object} params - option for request
     * @return {Promise}
     */
    saveIssueApi (params = {}) {
      return doRequest({
        url: `/${this.getSelectedScope}/maintenance/issues`,
        method: params.id ? 'PUT' : 'POST',
        data: params
      }, true)
    },

    /**
     * Delete Issue to server
     * @param {Object} params - option for request
     * @return {Promise}
     */
    deleteIssueApi (params = {}) {
      return doRequest({
        url: `/${this.getSelectedScope}/maintenance/issues`,
        method: 'DELETE',
        data: params
      }, true)
    },

    /**
     * Get all issue types
     * @return {Promise}
     */
    getIssueTypesApi () {
      return doRequest({
        url: `/${this.getSelectedScope}/maintenance/issues/types`,
        method: 'GET'
      }, true)
    },

    /**
     * Get all status types
     * @return {Promise}
     */
    getIssueStatusApi () {
      return doRequest({
        url: `/${this.getSelectedScope}/maintenance/status/types`,
        method: 'GET'
      }, true)
    },

    /**
     * Save Issue's files to server
     * @param {Object} params - option for request
     * @return {Promise}
     */
    saveIssueFilesApi (params = {}) {
      return doRequest({
        url: `/${this.getSelectedScope}/maintenance/files`,
        method: 'POST',
        data: params
      }, true)
    },

    /**
     * Save Issue's status to server
     * @param {Object} params - option for request
     * @return {Promise}
     */
    saveIssueStatusApi (params = {}) {
      return doRequest({
        url: `/${this.getSelectedScope}/maintenance/status`,
        method: 'POST',
        data: params
      }, true)
    },

    /**
     * Get Issue's address from issue by coordinates
     * @param {Object} params - issue's coordinates
     * @return {Promise}
     */
    getAddressByCoordinatesApi (params = {}) {
      return doRequest({
        url: `/${this.getSelectedScope}/maintenance/address`,
        method: 'POST',
        data: params
      }, true)
    },

    /**
     * Get issues from server and save them into vuex
     * @param {String} idScope - id scope
     * @param {Object} params - options to request
     * Options Params (filters):
     *  - start {date} - 2017-12-01T22:00:00Z
     *  - finish {date} - 2017-12-01T22:00:00Z
     *  - type {string} - "fuga"
     *  - assigned_user {number} - 1,2,3...
     *  - status {string} - "registrada", "cerrada"...
     *  - order_number {number} - 0001122
     */
    loadIssues (idScope, params = false) {
      this.showLoadingLayer({ show: true })
      this.getIssuesApi(idScope, params)
        .then((response) => {
          const data = get(response, 'data', false)
          if (data !== false) {
            // Save data in Vuex
            this.$store.dispatch(`${VUEX_MODULES.issue}/${UPDATE_ISSUE_ITEMS}`, data.features)
            // Load types and status from scope
            this.loadIssueTypes()
            this.loadIssueStatus()
          } else {
            // Show error
            this.showAlert({
              content: this.errorRequest,
              variant: 'danger'
            })
          }
        })
        .catch((response) => {
          // Show error
          this.showAlert({
            content: get(response, 'data.message', this.errorRequest),
            variant: 'danger'
          })
        })
        .finally(() => {
          this.showLoadingLayer({ show: false })
        })
    },

    /**
     * Get issue status from server and save them into vuex
     */
    loadIssueStatus () {
      this.getIssueStatusApi()
        .then((response) => {
          const issueStatus = response.data
            .reduce((status, state) => {
              status.push({
                value: state.id,
                text: this.$t(`view.issue.status.${state.id}`)
              })
              return status
            }, [])
          this.saveAllIssueStatus(issueStatus)
        })
    },

    /**
     * Get all app's available users and save into Vuex Object
     */
    loadUsers () {
      this.getUsersApi()
        .then((response) => {
          if (response.data && response.data.length > 0) {
            this.$store.dispatch(`${VUEX_MODULES.user}/${UPDATE_USERS}`, response.data)
          }
        })
    },

    /**
     * Get issue types from server and save them into vuex
     */
    loadIssueTypes () {
      this.getIssueTypesApi()
        .then((response) => {
          const issueTypes = response.data
            .reduce((types, type) => {
              types.push({
                value: type.id,
                text: this.$t(`view.issue.types.${type.id}`)
              })
              return types
            }, [])
          this.saveAllIssueTypes(issueTypes)
        })
    },

    /**
     * Save issue into vuex object and the server
     *
     * @param {Object} data - data issue object
     * @param {Object} redirect - redirect to ...
     */
    saveIssue (data, redirect = {}) {
      this.showLoadingLayer({ show: true })
      this.saveIssueApi(data)
        .then((response) => {
          const responseData = get(response, 'data', false)

          if (responseData !== false) {
            // Save data in Vuex (NEW)
            if (responseData.features && responseData.features.length > 0) {
              this.$store.dispatch(`${VUEX_MODULES.issue}/${UPDATE_ISSUE}`, responseData.features[0])
            }
            // Show success message
            this.showAlert({
              content: this.successForm,
              variant: 'success'
            })
            // Redirect to ...
            if (redirect !== {}) {
              this.$router.push(redirect)
            }
          } else {
            // Show error
            this.showAlert({
              content: get(response, 'data.message', this.errorForm),
              variant: 'danger'
            })
          }
        })
        .catch((response) => {
          // Show error
          this.showAlert({
            content: get(response, 'data.message', this.errorForm),
            variant: 'danger'
          })
        })
        .finally(() => {
          this.processingForm = false
          this.showLoadingLayer({ show: false })
        })
    },

    /**
     * Delete current issue
     * @param {Object} data - data object issue
     */
    deleteIssue (data = {}) {
      this.showLoadingLayer({ show: true })
      this.deleteIssueApi(data)
        .then((response) => {
          if (get(response, 'data', false) !== false) {
            // Remove in Vuex object
            this.$store.dispatch(`${VUEX_MODULES.issue}/${UPDATE_ISSUE_DELETE}`, data.id)
          }
        })
        .catch((response) => {
          // Show error
          this.showAlert({
            content: get(response, 'data.message', this.errorRequest),
            variant: 'danger'
          })
        })
        .finally(() => {
          this.showLoadingLayer({ show: false })
        })
    },

    /**
     * Save files associated to issue into vuex object
     * @param {Array} data - data array files issue
     */
    saveFilesIssue (data = []) {
      if (data !== []) {
        this.$store.dispatch(`${VUEX_MODULES.issue}/${UPDATE_ISSUE_FILE}`, data)
      }
    },

    /**
     * Save status associated to issue into vuex object
     * @param {Object} data - data object issue
     */
    saveStatusIssue (data = {}) {
      if (data !== {}) {
        this.$store.dispatch(`${VUEX_MODULES.issue}/${UPDATE_ISSUE_STATUS}`, data)
      }
    },

    /**
     * Save all differents issue status into vuex object
     * @param {Array} data - data object issue
     */
    saveAllIssueStatus (data = []) {
      if (Array.isArray(data) && data.length) {
        this.$store.dispatch(`${VUEX_MODULES.issue}/${UPDATE_ALL_ISSUE_STATUS}`, data)
      }
    },

    /**
     * Save all differents issue types into vuex object
     * @param {Array} data - data object issue
     */
    saveAllIssueTypes (data = []) {
      if (Array.isArray(data) && data.length) {
        this.$store.dispatch(`${VUEX_MODULES.issue}/${UPDATE_ALL_ISSUE_TYPES}`, data)
      }
    }

  }
}
