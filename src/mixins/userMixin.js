// Constants
import constants from '@/constants'
// Lib
import doRequest from '@/lib/request'
import md5 from 'md5'
// Vuex
import userActionsName from '@/store/modules/user/actionsName'

const { UPDATE_LOGGED_USER } = userActionsName
const { VUEX_MODULES } = constants

export default {
  methods: {
    /**
     * Login user in server OAUTH
     * @param {String} email - email's user
     * @param {String} password - password's user
     */
    loginUser (email, password) {
      return doRequest({
        url: '/auth/token/new',
        method: 'POST',
        data: { email, password: md5(password) }
      }, false)
    },
    /**
     * Set user data into Vuex
     * @param {Object} data - data user
     */
    setUserData (data) {
      this.$store.dispatch(`${VUEX_MODULES.user}/${UPDATE_LOGGED_USER}`, data)
    }
  }
}
