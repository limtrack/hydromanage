// Constants
import constants from '@/constants'
// Lib
import doRequest from '@/lib/request'
// Mixins
import interfaceMixin from '@/mixins/interfaceMixin'
// Vuex
import scopeActionsName from '@/store/modules/scope/actionsName'

const { UPDATE_SCOPE_ITEMS } = scopeActionsName
const { VUEX_MODULES } = constants
const get = require('lodash.get')

export default {
  mixins: [ interfaceMixin ],
  data () {
    return {
      verticalName: 'maintenance',
      errorRequest: this.$t('action.error.server')
    }
  },
  methods: {
    /**
     * Get Scopes from Server
     */
    getScopesApi () {
      return doRequest({
        url: '/admin/scopes',
        method: 'GET'
      }, true)
    },

    /**
     * Get scopes' logged user from server
     */
    loadScopes () {
      this.showLoadingLayer({ show: true })
      this.getScopesApi()
        .then((response) => {
          const data = get(response, 'data', false)

          if (data !== false) {
            const availableScopes = data.filter((scope) => {
              return scope.categories.includes(this.verticalName)
            })

            // Save data in Vuex
            this.$store.dispatch(`${VUEX_MODULES.scope}/${UPDATE_SCOPE_ITEMS}`, availableScopes)
          } else {
            // Show error
            this.showAlert({
              content: this.errorRequest,
              variant: 'danger'
            })
          }
        })
        .catch((response) => {
          // Show error
          this.showAlert({
            content: get(response, 'data.message', this.errorRequest),
            variant: 'danger'
          })
        })
        .finally(() => {
          this.showLoadingLayer({ show: false })
        })
    }
  }
}
