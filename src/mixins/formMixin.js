const get = require('lodash.get')

export default {
  data () {
    return {
      errorDefaultForm: this.$t('action.error.default'),
      processingForm: false,
      errorInForm: false
    }
  },
  computed: {
    /**
     * Check if all formulary fields are filled correctly
     * @return {boolean}
     */
    isFormCompleted () {
      if (this.$v.$params) {
        return !Object.keys(this.$v.$params)
          .some((key) => this.$v[key].$dirty && this.$v[key].$error)
      }
      return true
    }
  },
  methods: {
    /**
     * Default method to submit form.
     * It is possible that the child view has a owner method
     */
    onSubmit () {
      if (typeof this.beforeSubmit === 'function') {
        this.beforeSubmit()
          .then(() => {
            this.actionSubmit()
              .then(() => {
                if (typeof this.afterSubmit === 'function') {
                  this.afterSubmit()
                }
              })
          })
          .catch(() => {
            this.showErrorSubmit()
          })
      } else {
        this.actionSubmit()
          .then(() => {
            if (typeof this.afterSubmit === 'function') {
              this.afterSubmit()
            }
          })
      }
    },

    /**
     * Submit action
     * @return {promise}
     */
    actionSubmit () {
      const _this = this
      return new Promise((resolve, reject) => {
        _this.$v.$touch()
        if (!_this.$v.$invalid) {
          _this.processingForm = true
          resolve()
        } else {
          _this.showErrorSubmit()
          reject(this.errorForm || this.errorDefaultForm)
        }
      })
    },

    /**
     * Show error submit action
     */
    showErrorSubmit () {
      this.showAlert({
        content: this.errorForm || this.errorDefaultForm,
        variant: 'danger'
      })
    },

    /**
     * Get status input to attribute "state"
     * Only the states "null" (pristine) and "false" (error)
     * @param {string} field - name field in model
     * @return {null | false} Pristine or Error
     */
    getStatusInput (field) {
      const dirty = get(this, `$v.${field}.$dirty`, false)
      const error = get(this, `$v.${field}.$error`, false)

      if (dirty && error) {
        return false
      }
      return null
    },

    /**
     * Touch field input
     * @param {string} field - name field in model
     */
    touchField (field) {
      const currentField = get(this, `$v.${field}`, false)
      if (currentField) {
        currentField.$touch()
      }
    },

    /**
     * Give an object and a list of fields, return
     * a new object with the fields indicated (clean the object)
     * @param {Object} data - object to parser
     * @param {Array} fields - field's name
     * @return {Object}
     */
    customObject (data = {}, fields = []) {
      if (typeof data === 'object' && data) {
        return Object.keys(data)
          .reduce((parseObject, key) => {
            if (fields.includes(key)) {
              parseObject[key] = get(data, key, '')
            }
            return parseObject
          }, {})
      } else {
        return {}
      }
    }
  }
}
