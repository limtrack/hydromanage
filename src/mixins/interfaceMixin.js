import alertActions from '@/store/modules/alert/actionsName'
import modalActions from '@/store/modules/modal/actionsName'
import loadingLayerActions from '@/store/modules/loadingLayer/actionsName'
import scopeActions from '@/store/modules/scope/actionsName'
import issueActions from '@/store/modules/issue/actionsName'
import userActions from '@/store/modules/user/actionsName'
import mainActions from '@/store/actionsName'
import constants from '@/constants'

const { UPDATE_ALERT_MODEL } = alertActions
const { UPDATE_MODAL_MODEL } = modalActions
const { UPDATE_LOADING_LAYER_MODEL } = loadingLayerActions
const { UPDATE_RESET_SCOPE_MODEL } = scopeActions
const { UPDATE_RESET_ISSUE_MODEL } = issueActions
const { UPDATE_RESET_USER_MODEL } = userActions
const { UPDATE_RESET_MODEL } = mainActions
const { APP_MAIN_MODAL_ID, VUEX_MODULES } = constants

export default {
  data () {
    return {
      msgClosedSession: this.$t('action.user.sessionClosed')
    }
  },
  methods: {

    // SESSION

    /**
     * Close session deleting all data about user
     * in the "state" object and other models and show
     * alert with a mesagge
     */
    closeSession () {
      this.cleanSession()
      this.showAlert({
        content: this.msgClosedSession,
        variant: 'dark'
      })
    },

    /**
     * Clean session deleting all data about user
     * in the "state" object and other models
     */
    cleanSession () {
      this.$store.dispatch(`${VUEX_MODULES.scope}/${UPDATE_RESET_SCOPE_MODEL}`)
      this.$store.dispatch(`${VUEX_MODULES.user}/${UPDATE_RESET_USER_MODEL}`)
      this.$store.dispatch(`${VUEX_MODULES.issue}/${UPDATE_RESET_ISSUE_MODEL}`)
      this.$store.dispatch(UPDATE_RESET_MODEL)
    },

    // LAYOUT FUNCTIONS

    /**
     * Show an alert message
     * @param {Object} params - alert options
     */
    showAlert (params) {
      this.$store.dispatch(`${VUEX_MODULES.alert}/${UPDATE_ALERT_MODEL}`, params)
    },

    /**
     * Modify modal content
     * @param {Object} params - modal options content
     * @param {Object} options - modal options
     */
    modifyModal (params, options) {
      this.$store.dispatch(`${VUEX_MODULES.modal}/${UPDATE_MODAL_MODEL}`, params)
      if (options.show) {
        this.showModal(options.id)
      }
    },

    /**
     * Show modal
     * @param {String} modalId - modal id
     */
    showModal (modalId = APP_MAIN_MODAL_ID) {
      this.$root.$emit('bv::show::modal', modalId)
    },

    /**
     * Hide modal
     * @param {String} modalId - modal id
     */
    hideModal (modalId = APP_MAIN_MODAL_ID) {
      this.$root.$emit('bv::hide::modal', modalId)
    },

    /**
     * Modify loading layer content
     * @param {Object} params - loading layer options
     */
    showLoadingLayer (params) {
      this.$store.dispatch(`${VUEX_MODULES.loadingLayer}/${UPDATE_LOADING_LAYER_MODEL}`, params)
    },

    /**
     * Move us to differents position into nav history
     *
     * @param {*} steps - "steps" number that we want
     * move the history to forward (positive value)
     * or back (negative value)
     */
    goNavigationTo (steps = -1) {
      this.$router.go(steps)
    }
  }
}
