// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import constants from './constants'
// Filters
import './filters'
// I18n
import VueI18n from 'vue-i18n'
import i18nMessages from './locale/messages'
// Bootstrap
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// Uploader
import ImageUploader from 'vue-image-upload-resize'

// Uploader
Vue.use(ImageUploader)

// Bootstrap
Vue.use(BootstrapVue)

// I18n
Vue.use(VueI18n)
const messages = i18nMessages
const i18n = new VueI18n({
  locale: constants.DEFAULT_LANGUAGE,
  messages
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  i18n,
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
