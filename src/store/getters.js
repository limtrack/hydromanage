export default {
  /**
   * return referer URL before login view
   * @param state
   * @returns {Object}
   */
  getRefererURL: (state) => {
    const url = Object.keys(state.refererURL).length > 0 ? state.refererURL : false
    return url
  },

  /**
   * return token user
   * @param state
   * @returns {Object}
   */
  getToken: (state) => {
    return state.token || {}
  }
}
