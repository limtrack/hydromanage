import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import constants from '@/constants'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'
import initStore from './initStore'
// Modules
import alert from './modules/alert'
import modal from './modules/modal'
import loadingLayer from './modules/loadingLayer'
import user from './modules/user'
import scope from './modules/scope'
import issue from './modules/issue'

const { KEY_VUEX_LOCALSTORAGE, VUEX_MODULES } = constants
const state = initStore

Vue.use(Vuex)

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  plugins: [createPersistedState({key: KEY_VUEX_LOCALSTORAGE})],
  modules: {
    [VUEX_MODULES.alert]: {
      namespaced: true,
      ...alert
    },
    [VUEX_MODULES.modal]: {
      namespaced: true,
      ...modal
    },
    [VUEX_MODULES.loadingLayer]: {
      namespaced: true,
      ...loadingLayer
    },
    [VUEX_MODULES.user]: {
      namespaced: true,
      ...user
    },
    [VUEX_MODULES.scope]: {
      namespaced: true,
      ...scope
    },
    [VUEX_MODULES.issue]: {
      namespaced: true,
      ...issue
    }
  }
})
