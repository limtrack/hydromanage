import mutationsName from './mutationsName'
import actionsName from './actionsName'

export default {
  [actionsName.UPDATE_MODAL_MODEL]: ({ commit }, value) => {
    commit(mutationsName.SET_MODAL_MODEL, value)
  },

  [actionsName.UPDATE_MODAL_RESET_MODEL]: ({ commit }) => {
    commit(mutationsName.SET_MODAL_RESET_MODEL)
  }
}
