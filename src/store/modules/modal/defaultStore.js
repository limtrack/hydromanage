export default {
  title: '',
  content: '',
  component: '',
  params: {},
  className: '',
  showHeader: true,
  showFooter: true,
  showButtonOK: true,
  showButtonCancel: true,
  actionButtonOK: null,
  actionButtonCancel: null
}
