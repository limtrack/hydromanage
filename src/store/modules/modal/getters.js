export default {
  /**
   * return app modal attributes
   * @param state
   * @returns {Object}
   */
  getModal: (state) => state || {}
}
