import defaultStore from './defaultStore'
import mutationsName from './mutationsName'

export default {
  [mutationsName.SET_SCOPE_ITEMS]: (state, value) => {
    state.scopes = value
  },

  [mutationsName.SET_SCOPE_SELECTED]: (state, value) => {
    state.selected = value
  },

  [mutationsName.SET_RESET_SCOPE_MODEL]: (state) => {
    state = Object.assign(state, defaultStore)
  }
}
