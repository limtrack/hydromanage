export default {
  /**
   * return scopes' user
   * @param state
   * @returns {Object}
   */
  getScopes: (state) => state.scopes || [],

  /**
   * return scopes' user
   * @param state
   * @returns {Object}
   */
  getScopesLite: (state) => {
    return state.scopes.map((scope) => {
      return {
        id: scope.id,
        name: scope.name
      }
    })
  },

  /**
   * return selected scopes by user
   * @param state
   * @returns {Number}
   */
  getSelectedScope: (state) => state.selected || null
}
