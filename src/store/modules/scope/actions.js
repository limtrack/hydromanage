import mutationsName from './mutationsName'
import actionsName from './actionsName'

export default {

  /**
   * Update the scopes into the model
   */
  [actionsName.UPDATE_SCOPE_ITEMS]: ({ commit }, value) => {
    commit(mutationsName.SET_SCOPE_ITEMS, value)
  },

  /**
   * Update selected scope
   */
  [actionsName.UPDATE_SCOPE_SELECTED]: ({ commit }, value) => {
    commit(mutationsName.SET_SCOPE_SELECTED, value)
  },

  /**
   * Reset scope model
   */
  [actionsName.UPDATE_RESET_SCOPE_MODEL]: ({ commit }) => {
    commit(mutationsName.SET_RESET_SCOPE_MODEL)
  }
}
