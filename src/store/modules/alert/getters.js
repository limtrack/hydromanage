export default {
  /**
   * return class object alert app
   * @param state
   * @returns {Object}
   */
  getAlert: (state) => {
    return state || {}
  }
}
