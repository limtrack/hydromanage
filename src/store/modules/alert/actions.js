import mutationsName from './mutationsName'
import actionsName from './actionsName'

export default {
  [actionsName.UPDATE_ALERT_MODEL]: ({ commit }, value) => {
    commit(mutationsName.SET_ALERT_MODEL, value)
  },

  [actionsName.UPDATE_ALERT_RESET_MODEL]: ({ commit }) => {
    commit(mutationsName.SET_ALERT_RESET_MODEL)
  }
}
