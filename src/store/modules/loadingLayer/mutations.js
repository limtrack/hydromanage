import mutationsName from './mutationsName'
import defaultStore from './defaultStore'

export default {
  [mutationsName.SET_LOADING_LAYER_MODEL]: (state, value) => {
    state = Object.assign(state, value)
  },

  [mutationsName.SET_LOADING_LAYER_RESET_MODEL]: (state) => {
    state = Object.assign(state, defaultStore)
  }
}
