export default {
  /**
   * return class object loading app
   * @param state
   * @returns {Object}
   */
  getLoadingLayer: (state) => state || {}
}
