export default {
  loggedUser: {
    address: null,
    email: null,
    id: null,
    ldap: null,
    name: null,
    superadmin: null,
    surname: null,
    telephone: null
  },
  users: []
}
