export default {
  /**
   * return logged user status
   * @param state
   * @returns {Boolean}
   */
  isUserLogged: (state) => Boolean(state.loggedUser.id),

  /**
   * return logged user data
   * @param state
   * @returns {Object}
   */
  getLoggedUser: (state) => state.loggedUser || {},

  /**
   * return all users app
   * @param state
   * @returns {Object}
   */
  getUsers: (state) => state.users || []
}
