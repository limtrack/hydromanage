export default {
  /**
   * return issues' user
   * @param {Object} state
   * @returns {Object}
   */
  getIssues: (state) => state.issues || [],
  /**
   * return issue types
   * @param {Object} state
   * @returns {Object}
   */
  getIssueTypes: (state) => state.types || [],
  /**
   * return issue status
   * @param {Object} state
   * @returns {Object}
   */
  getIssueStatus: (state) => state.status || [],
  /**
   * return issue detail
   * @param {Object} state
   * @param {String} id - entity_id
   * @returns {Object}
   */
  getIssue: (state) => id => {
    if (id) {
      return state.issues.find((issue) => {
        if (issue.properties.id === id) {
          return issue
        }
      }) || {}
    }
    return {}
  }
}
