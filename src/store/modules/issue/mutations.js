import defaultStore from './defaultStore'
import mutationsName from './mutationsName'

const get = require('lodash.get')
const set = require('lodash.set')

export default {
  [mutationsName.SET_ISSUE_ITEMS]: (state, value) => {
    state.issues = value
  },

  [mutationsName.SET_ISSUE]: (state, value) => {
    state.issues.unshift(value)
  },

  [mutationsName.SET_ISSUE_DELETE]: (state, value) => {
    state.issues = state.issues.filter((issue) => {
      return value !== issue.properties.id
    })
  },

  [mutationsName.SET_ISSUE_FILE]: (state, value) => {
    const currentFiles = (Array.isArray(value))
      ? value
      : [value]

    state.issues.forEach((issue, key) => {
      if (issue.properties.id === currentFiles[0].id_issue) {
        // if propertie "files" doesn't exist then we create it
        if (!Array.isArray(get(state, `issues[${key}].properties.files`))) {
          set(state, `issues[${key}].properties.files`, [])
        }
        state.issues[key].properties.files.unshift(...currentFiles)
      }
    })
  },

  [mutationsName.SET_ISSUE_STATUS]: (state, value) => {
    state.issues.forEach((issue, key) => {
      if (issue.properties.id === value.id_issue) {
        // if propertie "status" doesn't exist then we create it
        if (!Array.isArray(get(state, `issues[${key}].properties.status`))) {
          set(state, `issues[${key}].properties.status`, [])
        }
        // Add status to status list
        state.issues[key].properties.status.unshift(value)
        // Current Status
        set(state, `issues[${key}].properties.current_status`, value.type)
      }
    })
  },

  [mutationsName.SET_ALL_ISSUE_STATUS]: (state, value) => {
    state.status = value
  },

  [mutationsName.SET_ALL_ISSUE_TYPES]: (state, value) => {
    state.types = value
  },

  [mutationsName.SET_RESET_ISSUE_MODEL]: (state) => {
    state = Object.assign(state, defaultStore)
  }
}
