import mutationsName from './mutationsName'
import actionsName from './actionsName'

export default {

  /**
   * Update the issues into the model
   */
  [actionsName.UPDATE_ISSUE_ITEMS]: ({ commit }, value) => {
    commit(mutationsName.SET_ISSUE_ITEMS, value)
  },

  /**
   * Update an issues into the model
   */
  [actionsName.UPDATE_ISSUE]: ({ commit }, value) => {
    commit(mutationsName.SET_ISSUE, value)
  },

  /**
   * Remove an issue from the model
   */
  [actionsName.UPDATE_ISSUE_DELETE]: ({ commit }, value) => {
    commit(mutationsName.SET_ISSUE_DELETE, value)
  },

  /**
   * Update the list files issues into the model
   */
  [actionsName.UPDATE_ISSUE_FILE]: ({ commit }, value) => {
    commit(mutationsName.SET_ISSUE_FILE, value)
  },

  /**
   * Update the issue status (one issue) list into the model
   */
  [actionsName.UPDATE_ISSUE_STATUS]: ({ commit }, value) => {
    commit(mutationsName.SET_ISSUE_STATUS, value)
  },

  /**
   * Update the issues' status list (all different status) into the model
   */
  [actionsName.UPDATE_ALL_ISSUE_STATUS]: ({ commit }, value) => {
    commit(mutationsName.SET_ALL_ISSUE_STATUS, value)
  },

  /**
   * Update the issues' types list (all different types) into the model
   */
  [actionsName.UPDATE_ALL_ISSUE_TYPES]: ({ commit }, value) => {
    commit(mutationsName.SET_ALL_ISSUE_TYPES, value)
  },

  /**
   * Reset issue model
   */
  [actionsName.UPDATE_RESET_ISSUE_MODEL]: ({ commit }) => {
    commit(mutationsName.SET_RESET_ISSUE_MODEL)
  }
}
