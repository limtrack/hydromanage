/**
 * This import way the components is to build differents
 * chunks and will do the lighter app
 **/

const ViewLogin = () => import('@/views/user/ViewLogin')
const ViewIssues = () => import('@/views/issue/ViewIssues')
const ViewIssueDetail = () => import('@/views/issue/ViewIssueDetail')
const ViewIssueAdd = () => import('@/views/issue/ViewIssueAdd')
const ViewExplore = () => import('@/views/explore/ViewExplore')
const ViewApkDownload = () => import('@/views/apk/ViewApkDownload')

export default [
  {
    path: '/',
    name: 'init',
    component: ViewIssues
  },
  {
    path: '/login/:scope?',
    name: 'login',
    component: ViewLogin,
    meta: {
      isPublic: true
    }
  },
  {
    path: '/issues',
    name: 'issues',
    component: ViewIssues
  },
  {
    path: '/issues/:id',
    name: 'issues-detail',
    component: ViewIssueDetail
  },
  {
    path: '/issue-add',
    name: 'issue-add',
    component: ViewIssueAdd
  },
  {
    path: '/explore/:id?',
    name: 'explore',
    component: ViewExplore
  },
  {
    path: '/apk',
    name: 'apk',
    component: ViewApkDownload,
    meta: {
      isPublic: true
    }
  }
]
