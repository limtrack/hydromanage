import Vue from 'vue'
import Router from 'vue-router'
// Routes
import routesName from './routesName'
// Constants
import constants from '@/constants'
// Others
import mainActions from '@/store/actionsName'
import store from '@/store'

Vue.use(Router)

const routes = routesName
const { LOGIN_PATH } = constants
const { UPDATE_REFERER_URL } = mainActions

const router = new Router({
  routes,
  scrollBehavior (to, from, savedPosition) {
    // Always on top when change
    // the path into the app
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  if ((to.meta && to.meta.isPublic) || store.getters['user/isUserLogged']) {
    next()
  } else {
    store.dispatch(UPDATE_REFERER_URL, to)
    next({ path: '/' + LOGIN_PATH })
  }
})

export default router
