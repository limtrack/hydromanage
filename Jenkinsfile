#!/usr/bin/env groovy

// Global Environment variables
FAILURE_EMAIL = "urbo.team@geographica.gs"
DESIRED_REPOSITORY = "https://github.com/GeographicaGS/AquaGis-app.git"


pipeline {
  agent { node {
    label 'master'
  } }
  options {
    ansiColor('xterm')
  }

  stages {
    stage('Preparing for build') {
      // agent{ docker{
      //     image 'debian'
      // } }
      agent { node {
        label 'master'
      } }
      steps {
        prepareBuild()
      }
    }
    stage('Building') {
      agent { node {
        label 'docker'
      } }
      steps {
        sh "docker build --pull=true -t geographica/aquasig-app_www:${git_commit} ."
      }
    }

    stage('Linter') {
      agent { node {
        label 'docker'
      } }
      steps {
        sh "docker run -i --rm geographica/aquasig-app_www:${git_commit} npm run-script lint"
      }
    }
    stage('Confirm deployment') {
      agent { node {
        label 'master'
      } }
      when { anyOf {
        branch 'master';
        // branch 'staging';
        // branch 'dev';
      } }
      steps {
        script {
          if ( env.BRANCH_NAME == "master" ) {
            env.DEPLOY_TYPE = "geo-carto"
            env.DEPLOY_TO = "prod"
            env.DEPLOY_URL = "aquasig-app.geographica.gs"
          }
        }
      }
    }
    stage("Deploy") {
      agent { node {
        label 'docker'
      } }
      when { expression {
        env.DEPLOY_TYPE != null
      } }
      steps {
        script {
          echo "Deploy type: ${env.DEPLOY_TYPE}"
          echo "Deploy to: ${env.DEPLOY_TO}"
          echo "Deploy url: ${env.DEPLOY_URL}"

          sh "docker run -i --rm  -v \$(pwd)/dist:/usr/src/app/dist geographica/aquasig-app_www:${git_commit} npm run-script build"

          if (env.DEPLOY_TYPE == "geo-carto") {
            sh "gcloud config set project geographica-gs"
            timeout(time:1, unit:'MINUTES') {
              sh "./deploy/sync_bucket.sh ${env.DEPLOY_URL} ./dist"
            }
          } else {
            error("Unknown DEPLOY_TYPE: '${env.DEPLOY_TYPE}'")
          }
        }
      }
    }
  }
  post {
    always {
      node('docker') {
        sh "sudo rm -rf ./*"
        deleteDir() /* clean up our workspace */
      }
      node('master') {
        deleteDir() /* clean up our workspace */
      }    }
    unstable {
      notifyStatus(currentBuild.currentResult)
    }
    failure {
      notifyStatus(currentBuild.currentResult)
    }
  }
}

def prepareBuild() {
  script {
    checkout scm

    sh "git rev-parse --short HEAD > .git/git_commit"
    sh "git --no-pager show -s --format='%ae' HEAD > .git/git_committer_email"

    workspace = pwd()
    branch_name = "${ env.BRANCH_NAME }".replaceAll("/", "_")
    git_commit = readFile(".git/git_commit").replaceAll("\n", "").replaceAll("\r", "")
    build_name = "${git_commit}"
    job_name = "${ env.JOB_NAME }".replaceAll("%2F", "/")
    committer_email = readFile(".git/git_committer_email").replaceAll("\n", "").replaceAll("\r", "")
    GIT_URL = sh(returnStdout: true, script: "git config --get remote.origin.url").trim()
    if ( GIT_URL != DESIRED_REPOSITORY ) {
      echo "!!!!!"
      error("This jenkinsfile is configured for '${ DESIRED_REPOSITORY }' but it was executed from '${ GIT_URL }'.")
    }
  }
}

def notifyStatus(buildStatus) {
  def status
  def send_to

  try {
    switch (branch_name) {
      case 'master':
        send_to = "${ committer_email }, ${ FAILURE_EMAIL }"
        break
      default:
        send_to = "${ committer_email }"
        break
    }
  } catch(Exception ex) {
    send_to = "${ FAILURE_EMAIL }"
  }

  echo "Sending error email to: ${ send_to }"
  try {
    mail  to: "${ send_to }",
          from: "Jenkins Geographica <system@geographica.gs>",
          subject: "[${ buildStatus }]   ${currentBuild.fullDisplayName}",
          body: "Something is wrong in '${currentBuild.fullDisplayName}'. \n\nSee ${env.BUILD_URL} for more details."
  } catch(Exception ex) {
    echo "Something was wrong sending error email :("
  }
}

