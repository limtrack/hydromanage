FROM node:10-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
COPY package*.json /usr/src/app/
RUN \
    set -x \
    && apk add --no-cache --virtual build-dep \
        git \
    # && npm ci \
    && npm install \
    && npm cache clean --force \
    && apk del build-dep
COPY . /usr/src/app

CMD [ "npm", "start" ]
