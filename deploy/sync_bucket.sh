#!/bin/bash
# CNAME -> c.storage.googleapis.com

bad_usage(){
    echo 'Bad parameters:'
    echo "Usage: $0 <domain> <folder_to_upload>"
    exit 1
}

check_bucket_exist() {
    gsutil ls gs://${DOMAIN} &> /dev/null
    echo $?
}

create_bucket(){
    gsutil mb -l eu gs://${DOMAIN}
    gsutil iam ch allUsers:objectViewer gs://${DOMAIN}
    gsutil web set -m index.html -e index.html gs://${DOMAIN}
}

sync(){
    echo ""
    echo "Synchronizing files..."
    gsutil -m rsync -x "deploy\/.*$|\.git\/.*$|Jenkinsfile$|Dockerfile$" -r -d ${FOLDER} gs://${DOMAIN}
}

if  [ $# -ne 2 ] ; then
    bad_usage
    exit 1
fi

DOMAIN=$1
FOLDER=$2

if [ "$(check_bucket_exist)" -ne 0 ]; then
    echo "Bucket doesn't exist, creating . . . ."
    create_bucket
fi

sync
