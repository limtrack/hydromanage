module.exports = {
  NODE_ENV: '"production"',
  API_ENDPOINT: '"https://urbo-aquagis-backend.geographica.gs/api"', // API Endpoint
  MAPBOX_TOKEN: '"pk.eyJ1Ijoiam9zbW9yc290IiwiYSI6ImNqYXBvcW9oNjVlaDAyeHIxejdtbmdvbXIifQ.a3H7tK8uHIaXbU7K34Q1RA"' // Mapbox Token
}
